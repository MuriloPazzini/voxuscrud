$(function () {
  var operation = "C"; //"C"=Create
  var selected_index = -1; // Indice do elemento na tabela
  var Task = localStorage.getItem("Task"); //Retorna os dados armazenados
  Task = JSON.parse(Task); //Conversão String-Object
  var StoredUser = localStorage.getItem("User");
  StoredUser = JSON.parse(StoredUser);
  if (Task === null) // Inicia vetor se já não foi iniciado
      Task = [];

  function Create() {
    //Obtém os valores na forma html e passa pra JSON
    var TaskUnidade = JSON.stringify({
      ID_Task: $("#txtID").val(),
      Priority_Task: $("#txtPriority").val(),
      Task_Name: $("#txtName").val(),
      Task_Description: $("#txtDesc").val(),
      Email: StoredUser[0].Email,
      Attached_Files: $("#image-file").val(),
      Status: "Not Done",
      StatusDefinedBy: StoredUser[0].Email
    }); 
    //Push
    Task.push(TaskUnidade);
    //Armazenar localmente os dados
    localStorage.setItem("Task", JSON.stringify(Task));
    alert("Os dados foram armazenados"); //Mensagem de alerta
    return true;
  }

  function Edit() {
    var obj = JSON.parse(Task[selected_index]);
    var StatusTemp = obj.Status;
    var StatusDefinedByTemp = obj.StatusDefinedBy;
    // Editar o item selecionado na table
    Task[selected_index] = JSON.stringify({
        ID_Task: $("#txtID").val(),
        Priority_Task: $("#txtPriority").val(),
        Task_Name: $("#txtName").val(),
        Task_Description: $("#txtDesc").val(),
        Email: StoredUser[0].Email,
        Attached_Files: $("#image-file").val(),
        Status: StatusTemp,
        StatusDefinedBy: StatusDefinedByTemp
    });
    //Armazenar localmente os dados
    localStorage.setItem("Task", JSON.stringify(Task)); 
    alert("Os dados foram editados"); //Mensagem de alerta
    return true;
  }

  function Delete() {
    //Eliminar o elemento seleccionado na tabela
    Task.splice(selected_index, 1); 
    //Armazenar localmente os dados
    localStorage.setItem("Task", JSON.stringify(Task)); 
    alert("Os dados foram Eliminados"); //Mensagem de alerta
  }

  function MarkDone() {
    var obj = JSON.parse(Task[selected_index]);
    if (obj.Status != "Done"){
     obj.Status = "Done"; 
    }
    else{
     obj.Status = "Not Done"; 
    }
    Task[selected_index] = JSON.stringify({
      ID_Task: obj.ID_Task,
      Priority_Task: obj.Priority_Task,
      Task_Name: obj.Task_Name,
      Task_Description: obj.Task_Description,
      Email: StoredUser[0].Email,
      Attached_Files: obj.Attached_Files,
      Status: obj.Status,
      StatusDefinedBy: StoredUser[0].Email
    });
    //Armazenar localmente os dados
    localStorage.setItem("Task", JSON.stringify(Task)); 
    alert("Status Alterado"); //Mensagem de alerta
    $("#txtName").focus();
    return true;
  }

  function List() {
    $("#tblList").html("");
    $("#tblList").html(
            "<thead>" +
            "<tr>" +                
            "<th>ID Task</th>" +
            "<th>Prioridade</th>" +
            "<th>Nome Task</th>" +
            "<th>Descrição</th>" +
            "<th>Submetido por</th>" +
            "<th>Anexos</th>" +
            "<th>Status</th>" +
            "<th>Sinalizado por</th>" +
            "<th>Ações</th>" +
            "</tr>" +
            "</thead>" +
            "<tbody>" +
            "</tbody>"
            ); //Colocando table no HTML
    for (var i in Task) {
        var obj = JSON.parse(Task[i]);
        $("#tblList tbody").append("<tr>" +                    
                "<td>" + obj.ID_Task + "</td>" +
                "<td>" + obj.Priority_Task + "</td>" +
                "<td>" + obj.Task_Name + "</td>" +
                "<td>" + obj.Task_Description + "</td>" +
                "<td>" + obj.Email + "</td>" + 
                "<td>" + obj.Attached_Files + "</td>" +  
                "<td>" + obj.Status + "</td>" + 
                "<td>" + obj.StatusDefinedBy + "</td>" +                  
                "<td><img src='edit.png' alt='Edit" + i + "' class='btnEdit'/>&nbsp &nbsp<img src='delete.png' alt='Delete" + i + "' class='btnDelete'/>&nbsp &nbsp<img src='done.png' alt='Done" + i + "' class='btnDone'/></td>" +
                "</tr>"
                );
    } //Retrieve e coloca os itens na tabela
  }

  $("#frmTask").bind("submit", function () {
    if (operation === "C")
        return Create();
    else
        return Edit();
  });
  
  List();

  $(".btnEdit").off("click").on("click", function (e) {
    operation = "E"; //"E" = Editar
    //Obtém o index do item a ser editado
    selected_index = parseInt($(this).attr("alt").replace("Edit", ""));
    // Converte JSON para formato a ser alterado
    var obj = JSON.parse(Task[selected_index]); 
    $("#txtID").val(obj.ID_Task);
    $("#txtPriority").val(obj.Priority_Task);
    $("#txtName").val(obj.Task_Name);
    $("#txtDesc").val(obj.Task_Description);
    $("#image-file").val(obj.Attached_Files);
    $("#txtID").attr("readonly", "readonly");
    $("#txtName").focus();
  });

  $(".btnDelete").off("click").on("click", function (e) {
    //Obtém o index do item a ser editado
    selected_index = parseInt($(this).attr("alt").replace("Delete", "")); 
    Delete(); //Eliminar o item
    List();
    location.reload(true); //Lista de novo atualizado
  });

    $(".btnDone").off("click").on("click", function (e) {
    //Obtém o index do item a ser editado
    selected_index = parseInt($(this).attr("alt").replace("Done", "")); 
    MarkDone();
    List();
    location.reload(true); //Lista de novo atualizado

  });
});

