● Link com acesso para código no repositório GIT ou equivalente
https://id.atlassian.com/signup/invite?application=bitbucket&continue=https%3A//bitbucket.org/socialauth/login/atlassianid/?next%3Dhttps%3A//bitbucket.org/invitations/repository/CCDdnM1cClV97FrT7WErsa4o2bX0BkUP&signature=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZSI6Imludml0ZSIsImV4cCI6MTUxMjA0MjAxMSwiaWF0IjoxNTExNDM3MjExLCJzdWIiOiJtdWNhXzUwQGhvdG1haWwuY29tIiwiaXNzIjoiYWNjb3VudCJ9.NOMRYPYi5gSacBQ6TYlbIF8Ai3PA_qL5zN1KeWsAFgE&source=a1ece0d337deaac61592478f9b0ab9a7
https://bitbucket.org/MuriloPazzini/voxuscrud

● Instruções para testarmos o seu código (ele deve rodar pelo menos
localmente!)

O projeto roda localmente, ele faz uso de JSON para armazenar os dados já que seria impraticavel manter um servidor caseiro ou alugar um para finalizar o desafio.

● Arquivo no repositório reportando, para cada parte desenvolvida :
○ quanto tempo você demorou

Parte 1 - Foi relativamente simples, devo ter demorado cerca de 2 horas, mesmo estando um pouco enferrujado no JavaScript, já que uso somente DelphiXE8 no 
meu trabalho atual.

Parte 2 - Foi onde passei mais tempo, apesar de ser algo simples, devo ter demorado cerca de 3 à 4 horas para concluir essa parte, pois assumi que estava
retornando erro por erro de autenticação das minhas credenciais do Google, quando na verdade minhas configurações iniciais estavam corretas e o erro era
que o projeto não estava sendo hospedado por um servidor local, após configurar um servidor local simples com python 3, o projeto rodou sem problemas.

Parte 3 - Um pouco trabalhoso, levando a alguns erros por falta de atenção e muita replicação de código da minha parte, porém demorei cerca de 1 à 2 horas.

Parte 4 - A parte mais simples do projeto todo, fiquei surpreso por ter um passo somente pra essa inclusão, levei cerca de 30 minutos pra concluir e testar.

Parte 5 - Acredito não ser possível, pois uma das requisições para o uso de elastic search e armazenamento requer um 'identity pool' que não pode ser gerado
para a area de São Paulo - Br (sa-east-1), conforme informado no link: https://console.aws.amazon.com/ecs/home?region=sa-east-1#

● As assumptions que você fez para o desenvolvimento :

Assumi que o servidor poderia ser local e que quando vocês usariam a porta 8080 para testar.

